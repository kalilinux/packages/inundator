install:
	install -v -d -m 0755 $(DESTDIR)/usr/bin/
	install -v -m 0755 inundator.pl $(DESTDIR)/usr/bin/inundator
	install -v -d -m 0755 $(DESTDIR)/usr/share/man/man1/
	cat inundator.1 | gzip > $(DESTDIR)/usr/share/man/man1/inundator.1.gz
